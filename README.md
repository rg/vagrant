# Vagrant

My usual Vagrantfiles. `bash bootstrap.sh` should get you installed with Vagrant + VirtualBox and some plugins.

## Vagrant installation

```bash
sudo apt-get install -y curl git
curl -O $(curl -s -L https://www.vagrantup.com/downloads.html | egrep "debian.*$(arch)" | cut -d'"' -f8)
sudo apt-get install -y ./vagrant*.deb
rm -f vagrant*.deb
vagrant plugin install vagrant-hostmanager
cat <<EOSUDOER | sudo tee /etc/sudoers.d/vagrant_hostmanager
Cmnd_Alias VAGRANT_HOSTMANAGER_UPDATE = /usr/bin/cp $HOME/.vagrant.d/tmp/hosts.local /etc/hosts
%sudo ALL=(root) NOPASSWD: VAGRANT_HOSTMANAGER_UPDATE
EOSUDOER
```

## Provider installation

### VirtualBox

VirtualBox is the default. But it's maintained by Oracle. 

```bash
sudo apt-get install -y virtualbox
vagrant plugin install vagrant-vbguest
cat <<EOSUDOER | sudo tee /etc/sudoers.d/vagrant_nfs
# Allow passwordless startup of Vagrant when using NFS.
Cmnd_Alias VAGRANT_EXPORTS_CHOWN = /usr/bin/chown 0\:0 /tmp/*
Cmnd_Alias VAGRANT_EXPORTS_MV = /usr/bin/mv -f /tmp/* /etc/exports
Cmnd_Alias VAGRANT_NFSD_CHECK = /etc/init.d/nfs-kernel-server status
Cmnd_Alias VAGRANT_NFSD_START = /etc/init.d/nfs-kernel-server start
Cmnd_Alias VAGRANT_NFSD_APPLY = /usr/sbin/exportfs -ar
%sudo ALL=(root) NOPASSWD: VAGRANT_EXPORTS_CHOWN, VAGRANT_EXPORTS_MV, VAGRANT_NFSD_CHECK, VAGRANT_NFSD_START, VAGRANT_NFSD_APPLY
EOSUDOER
```

### Libvirt

Libvirt over KVM & QEMU is a geek alternative.

```bash
sudo apt-get install -y build-dep vagrant vagrant-hostmanager vagrant-libvirt  qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils nfs-kernel-server
echo "export VAGRANT_DEFAULT_PROVIDER=libvirt" | tee -a ~/.zshrc | tee -a ~/.bashrc
```

## Get Started

Get this repository

    git clone https://framagit.org/rg/vagrant.git

Example starting centos7

    cd vagrant/centos_7
    vagrant up
