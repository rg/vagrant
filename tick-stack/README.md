# TICK stack

This is a quick test of open source ansible roles to install the TICK stack, a.k.a. Telegraf + InfluxDB + Chronograf + Kapacitor

```
vagrant up
firefox http://tick.vagrant:8888/
```
