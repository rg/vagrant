#!/usr/bin/env bash

sudo vboxconfig
sudo update-secureboot-policy --enroll-key
sudo kmodsign sha512 /var/lib/shim-signed/mok/MOK.priv /var/lib/shim-signed/mok/MOK.der /lib/modules/$(uname -r)/updates/dkms/vboxdrv.ko
sudo systemctl restart virtualbox
sudo systemctl status virtualbox
