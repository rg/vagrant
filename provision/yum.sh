#!/bin/bash
# Setting up my usual in centos or redhat or fedora vagrant boxes

echo "Yum ..."
sudo yum install -y -q epel-release deltarpm
sudo yum clean all
sudo yum update -y -q
sudo yum install -y -q "*bin/pip3" bash-completion curl dos2unix git mlocate netcat nmap strace tee tmux tree vim wget zsh
