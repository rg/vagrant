#!/bin/bash
# Yunohost installation https://yunohost.org/#/install_manually_en

sudo export DEBIAN_FRONTEND='noninteractive'

echo "Yunohost..."
wget https://install.yunohost.org -O install_yunohost.sh
chmod +x install_yunohost.sh
sudo ./install_yunohost.sh -a
sudo yunohost tools postinstall --domain $(hostname -f) --password vagrant --force-password --ignore-dyndns
sudo yunohost --admin-password vagrant user create yunohost --firstname YunoHost --lastname Vagrant --mail vagrant@yunohost.vagrant --password YunoHost?

echo "Done. Open http://yunohost.vagrant"
echo "- Default user is 'yunohost', password 'YunoHost?'"
echo "- Admin password is 'vagrant'"
