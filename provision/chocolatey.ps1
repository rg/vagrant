# Turn off Chocolatey prompts
chocolatey feature enable -n=allowGlobalConfirmation

# Install BoxStarter
choco install BoxStarter

# Turn on Chocolatey prompts
# chocolatey feature disable -n=allowGlobalConfirmation
