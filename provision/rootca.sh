# Company certificate in rootCA to validate self-signed stuff
sudo tee /etc/pki/ca-trust/source/anchors/company.der <<B64DER
-----BEGIN CERTIFICATE-----
MIIDrzCCApegAwIBAgIQctBLdKkxF5RJpOn2dgJ3YTANBgkqhkiG9w0BAQ0FADBL
3SXnW7YeUaXXXXXXXXXXXXXXXXxxxxxxxxxxXXXXXXXXxxxx13fxK27fdJ2KQhw8
9cmrzgLFeK2FJBDH5dIgPLHpzlL5NtoLThAx9OnMu1VyM74=
-----END CERTIFICATE-----
B64DER
sudo update-ca-trust enable
sudo update-ca-trust extract
