#!/bin/ash
# Setting up my usual in alpine boxes

# sudo cat <<ETH1 >> /etc/network/interfaces
#
# auth eth1
# iface eth1 inet static
#     address 192.168.100.100
#     netmask 255.255.255.0
#     gateway 192.168.33.1
# ETH1
# # sudo ifup eth1

echo "Apk..."
sudo apk update -q
sudo apk upgrade -q
sudo apk add -q bash-completion curl dos2unix findutils git glances htop man mlocate netcat netcat-openbsd nmap python3-pip strace tee tmux tree vim wget zsh
