#!/bin/bash
# Common stuff for linux distro, requires {yum,apt,apk}.sh for git, curl...

echo "Oh My Zsh..."
sudo sh -c "$(curl -qfsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" 1>/dev/null
echo "bindkey '\e[5~' history-beginning-search-backward" | sudo tee -a /root/.zshrc
echo "bindkey '\e[6~' history-beginning-search-forward" | sudo tee -a /root/.zshrc
echo "bindkey '\e[A' history-beginning-search-backward" | sudo tee -a /root/.zshrc
echo "bindkey '\e[B' history-beginning-search-forward" | sudo tee -a /root/.zshrc

echo "Git..."
sudo cp /vagrant/provision/gitconfig ~/.gitconfig &>/dev/null
# Switching git push settings to git 1.x compatible value if needed
if [ $(git --version | grep -q " 1.") ]
then
    sed -i -e "s;default = simple;default = current;" /root/.gitconfig
fi

echo "Vim..."
sudo cp /vagrant/provision/vimrc /etc/vimrc &>/dev/null

echo "Python's stuff"
sudo pip3 install -U -r /vagrant/provision/requirements.txt

# # Checkout all my github repositories at once
# echo "My git repos..."
# cd /opt
# REPOSITORIES=$(sudo curl -s https://api.github.com/users/rgarrigue/repos?type=owned | grep full_name | cut -d'"' -f 4)
# for R in $REPOSITORIES
# do
#   if [ ! -d "/opt/$R" ]; then
#     sudo git clone https://github.com/$R
#   else
#     cd /opt/$R
#     sudo git pull
#     cd - 1>/dev/null
#   fi
# done

echo "Updatedb for locate ..."
sudo updatedb
echo "Configuring sudoer.d for passwordless 'sudo updatedb'..."
echo "%sudo ALL=(root) NOPASSWD: $(which updatedb)" | sudo tee /etc/sudoers.d/updatedb

echo "Generic passwordless SSH key"
if [ ! -f "/root/.ssh/id_ed25519" ]
then
	sudo ssh-keygen -t ed25519 -q -N '' -f ~/.ssh/id_ed25519
fi
