#!/bin/bash
# Setting up my usual in debian or ubuntu vagrant boxes

echo "Apt..."
sudo apt-get update
sudo DEBIAN_FRONTEND='noninteractive' apt-get install --yes python3-pip python3-setuptools bash-completion curl dos2unix git ffmpeg mlocate netcat nmap strace sudo tmux tree vim-nox wget zsh virt-what dh-autoreconf ca-certificates silversearcher-ag
# Upgrading takes too much time from home, will do manually if needed
# sudo DEBIAN_FRONTEND='noninteractive' apt-get upgrade --yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"
