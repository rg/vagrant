#!/bin/sh

if which apt-get &> /dev/null; then

sudo apt-get install -y curl git vagrant virtualbox 
curl -O $(curl -s -L https://www.vagrantup.com/downloads.html | egrep "debian.*$(arch)" | cut -d'"' -f8)
sudo apt-get install -y ./vagrant*.deb 
rm -f vagrant*.deb

cat <<EOSUDOER | sudo tee /etc/sudoers.d/vagrant_nfs
# Allow passwordless startup of Vagrant when using NFS.
Cmnd_Alias VAGRANT_EXPORTS_CHOWN = /bin/chown 0\:0 /tmp/*
Cmnd_Alias VAGRANT_EXPORTS_MV = /bin/mv -f /tmp/* /etc/exports
Cmnd_Alias VAGRANT_NFSD_CHECK = /etc/init.d/nfs-kernel-server status
Cmnd_Alias VAGRANT_NFSD_START = /etc/init.d/nfs-kernel-server start
Cmnd_Alias VAGRANT_NFSD_APPLY = /usr/sbin/exportfs -ar
%sudo ALL=(root) NOPASSWD: VAGRANT_EXPORTS_CHOWN, VAGRANT_EXPORTS_MV, VAGRANT_NFSD_CHECK, VAGRANT_NFSD_START, VAGRANT_NFSD_APPLY
EOSUDOER

vagrant plugin install vagrant-hostmanager
cat <<EOSUDOER | sudo tee /etc/sudoers.d/vagrant_hostmanager
Cmnd_Alias VAGRANT_HOSTMANAGER_UPDATE = /bin/cp $HOME/.vagrant.d/tmp/hosts.local /etc/hosts
%sudo ALL=(root) NOPASSWD: VAGRANT_HOSTMANAGER_UPDATE
EOSUDOER

vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-windows

else

echo "apt-get is missing, exiting"
exit 1

fi
